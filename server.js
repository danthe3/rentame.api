const express = require("express");
const port = process.env.PORT || 8080;
const cors = require("cors");

const productsApi = require("./App/api/products");
const rentsApi = require("./App/api/rents");
const loginApi = require("./App/api/login");

var app = express();
app.use(cors());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/products", productsApi);
app.use("/rents", rentsApi);
app.use("/login", loginApi);

app.get("/*", (req, res) => {
  res.send("<h3>Rentame server Online!</h3>");
});
/*  */
app.listen(port, function () {
  console.log(`Online on port ${port}!`);
});
