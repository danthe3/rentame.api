const express = require("express");
const router = express.Router();
const dbClient = require("../Config/Connection");

router.post("/", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    dbo
      .collection("users")
      .findOne({ userName: req.body.userName, password: req.body.password })
      .then((result) => {
        console.log("user: " + result.userName + " logged in");
        const { firstName, lastName } = result;
        res.send({ firstName, lastName });
        db.close();
      })
      .catch((err) => {
        res.send(err);
        db.close();
      });
  });
});

module.exports = router;
