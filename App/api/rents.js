const express = require("express");
const router = express.Router();
const dbClient = require("../Config/Connection");

router.get("/", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    dbo
      .collection("rent_products")
      .find({})
      .toArray(function (err, result) {
        if (err) throw err;
        res.send(result);
        db.close();
      });
  });
  console.log("Consulta correcta -rentas-");
});

router.post("/", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    dbo.collection("rent_products").insertOne(req.body, function (err, res) {
      if (err) throw err;
      console.log("producto rentado productId: " + req.body.productId);
      db.close();
    });
  });
  res.send("producto rentado productId: " + req.body.productId);
});

module.exports = router;
