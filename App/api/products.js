const express = require("express");
const router = express.Router();
const dbClient = require("../Config/Connection");

router.get("/", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    dbo
      .collection("products")
      .find({})
      .toArray(function (err, result) {
        if (err) throw err;
        res.send(result);
        db.close();
        console.log("Consulta correcta -Products-");
      });
  });
});

router.get("/featured", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    var query = { isFeatured: "true" };
    dbo
      .collection("products")
      .find(query)
      .toArray(function (err, result) {
        if (err) throw err;
        res.send(result);
        db.close();
        console.log("Consulta correcta -Featured products-");
      });
  });
});

router.get("/:productId", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    dbo
      .collection("products")
      .findOne({ productId: req.params.productId }, function (err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);
        db.close();
        console.log("Consulta correcta productId: " + req.params.productId);
      });
  });
});

router.get("/find/:_id", (req, res) => {
  let { ObjectId } = require("mongodb");
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    let o_id = new ObjectId(req.params._id);
    dbo.collection("products").findOne({ _id: o_id }, function (err, result) {
      if (err) throw err;
      res.send(result);
      db.close();
      console.log("Consulta correcta _id: " + req.params._id);
    });
  });
});

router.post("/add", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    dbo
      .collection("products")
      .insertOne(req.body)
      .then(() => {
        console.log("producto insertado");
        res.send("Producto puesto en renta");
        db.close();
      })
      .catch((err) => {
        res.send(err);
        console.log(err);
        db.close();
      });
  });
});

router.put("/", (req, res) => {
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    var myquery = { productId: req.body.productId };
    var newValues = {
      $set: {
        title: req.body.title,
        description: req.body.description,
        img: req.body.img,
        img_alt: req.body.img_alt,
        productId: req.body.productId,
        isFeatured: req.body.isFeatured,
        price: req.body.price,
        brand: req.body.brand,
        status: req.body.status,
        availableStart: req.body.availableStart,
        availableEnd: req.body.availableEnd,
      },
    };
    dbo
      .collection("products")
      .updateOne(myquery, newValues, function (err, res) {
        if (err) throw err;
        console.log("producto actualizado");
        db.close();
        res.send("producto actualizado");
      });
  });
});

router.delete("/", (req, res) => {
  console.log(req.body);
  dbClient.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("rentame");
    var myquery = { product: req.body.productId };
    dbo.collection("products").deleteOne(myquery, function (err, obj) {
      if (err) throw err;
      console.log("producto eliminado");
      db.close();
      res.send("producto eliminado");
    });
  });
});

module.exports = router;
